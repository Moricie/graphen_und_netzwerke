import java.util.ArrayList;

public class Graph_adjazenz_list {
    private int node_count;
    public ArrayList<Node>[] node_lists;

    public Graph_adjazenz_list(int node_count)
    {
        this.node_count = node_count;
        node_lists = new ArrayList[node_count];
        for(int i = 0; i < node_count; i++)
        {
            node_lists[i] = new ArrayList<Node>();
        }
    }

    public Graph_adjazenz_list(Graph_adjazenz_list alist)
    {
        this.node_lists = alist.node_lists;
        this.node_count = alist.getNode_count();
    }

    public int getNode_count() {
        return node_count;
    }

    public void print_preds()
    {
        for(int i = 0; i < node_count; i++)
        {
            Node n = node_lists[i].get(0);
            if(n.getPred() != null)
            {
                System.out.println("N: " + n.getLabel() + " <- " + n.getPred().getLabel() + " K: " + n.getDisc());
            }
            else
            {
                System.out.println("N: " + n.getLabel() + " <- " + null);
            }
        }
    }
}
