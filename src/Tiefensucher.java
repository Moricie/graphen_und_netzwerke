import java.security.AllPermission;
import java.util.ArrayList;
import java.util.Stack;

public class Tiefensucher {
    private Stack<Node> stack;
    private int time;
    private ArrayList<ArrayList<Node>> top_sorts;
    private ArrayList<ArrayList<Node>> starke_z_k_sort;
    private int num_sorts;

    public Tiefensucher()
    {
        this.stack = new Stack<Node>();
        this.time = 0;
        this.top_sorts = new ArrayList<ArrayList<Node>>();
        this.starke_z_k_sort = new ArrayList<ArrayList<Node>>();
        this.num_sorts = 0;
    }

    public ArrayList<ArrayList<Node>> tiefensuche(Graph_adjazenz_list list)
    {
        this.top_sorts.clear();
        this.time = 0;
        this.num_sorts = 0;

        for(ArrayList<Node> l : list.node_lists)
        {

            Node u = l.get(0);
            if (!u.isVisited())
            {
                top_sorts.add(new ArrayList<Node>());
                visit(list, u, top_sorts);
                num_sorts++;
            }
        }
        // Output
        System.out.println("\nTiefensuche");
        for(ArrayList<Node> l : list.node_lists)
        {
            Node n = l.get(0);
            if(n.getPred() != null)
            {
                System.out.println(n.getLabel() + ", D: " + n.getDisc() + ", F: " + n.getFinish() + ", Pred: " + n.getPred().getLabel());
            }
            else {
                System.out.println(n.getLabel() + ", D: " + n.getDisc() + ", F: " + n.getFinish());
            }
        }
        // Top Sort
        System.out.println("\nTop Sort");
        for (ArrayList<Node> l: top_sorts)
        {
            System.out.println("\n");
            for (Node n: l)
            {
                    System.out.print(n.getLabel() + ", ");
            }
        }
        System.out.println("\n");

        return this.top_sorts;
    }

    public ArrayList<ArrayList<Node>> starke_z_k(Graph_adjazenz_list alist, ArrayList<ArrayList<Node>> top_sorts_1)
    {
        this.starke_z_k_sort.clear();
        this.time = 0;
        this.num_sorts = 0;

        for(int i = top_sorts_1.size()-1; i >= 0; i--)
        {
            ArrayList<Node> top_sort = top_sorts_1.get(i);
            for (int j = 0; j < top_sort.size(); j++)
            {
                Node u = alist.node_lists[top_sort.get(j).getLabel()-1].get(0);
                if (!u.isVisited())
                {
                    starke_z_k_sort.add(new ArrayList<Node>());
                    visit(alist, u, starke_z_k_sort);
                    num_sorts++;
                }
            }
        }
        // Output
        System.out.println("\nTiefensuche");
        for(ArrayList<Node> l : alist.node_lists)
        {
            Node n = l.get(0);
            if(n.getPred() != null)
            {
                System.out.println(n.getLabel() + ", D: " + n.getDisc() + ", F: " + n.getFinish() + ", Pred: " + n.getPred().getLabel());
            }
            else {
                System.out.println(n.getLabel() + ", D: " + n.getDisc() + ", F: " + n.getFinish());
            }
        }
        // Top Sort
        System.out.println("\nTop Sort");
        for (ArrayList<Node> l: starke_z_k_sort)
        {
            System.out.println("\n");
            for (Node n: l)
            {
                System.out.print(n.getLabel() + ", ");
            }
        }
        System.out.println("\n");

        return this.starke_z_k_sort;
    }

    private void visit(Graph_adjazenz_list list, Node u, ArrayList<ArrayList<Node>> top_ss)
    {
        time += 1;
        u.setVisited(true);
        u.setDisc(time);
        stack.push(u);

        while(!stack.empty())
        {
            u = stack.peek();
            int i = 0;
            boolean found = false;
            for (Node v: list.node_lists[u.getLabel()-1])
            {
                // If unvisited node found push it on the stack.
                if (!v.isVisited() && i > 0)
                {
                    v.setVisited(true);
                    time += 1;
                    v.setDisc(time);
                    v.setPred(u);
                    stack.push(v);
                    found = true;
                    break;
                    //visit(list, v);
                }

                i++;
            }
            // If nothing found finish node on top of stack.
            if(!found)
            {
                time += 1;
                u.setFinish(time);
                top_ss.get(num_sorts).add(0, u);
                stack.pop();
            }

        }

    }


}
