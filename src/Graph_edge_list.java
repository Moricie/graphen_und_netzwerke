import java.util.ArrayList;

public class Graph_edge_list {
    public ArrayList<Edge> edges;
    private int node_count;

    public Graph_edge_list(int node_count)
    {
        this.node_count = node_count;
        edges = new ArrayList<Edge>();
    }

    public Graph_edge_list(Graph_edge_list elist)
    {
        this.edges = elist.edges;
        this.node_count = elist.getNode_count();
    }

    public int getNode_count() {
        return node_count;
    }

    public Edge getEdge(int n0_label, int n1_label, boolean gerichtet)
    {
        for(int i = 0; i < edges.size(); i++)
        {
            Edge edge = edges.get(i);
            if (gerichtet)
            {
                if(edge.getN0().getLabel() == n0_label && edge.getN1().getLabel() == n1_label)
                {
                    return edge;
                }
            }
            else
            {
                if(edge.getN0().getLabel() == n0_label && edge.getN1().getLabel() == n1_label || edge.getN0().getLabel() == n1_label && edge.getN1().getLabel() == n0_label)
                {
                    return edge;
                }
            }
        }
        return null;
    }

    public int get_graph_weight()
    {
        int weight = 0;
        for(int i = 0; i < edges.size(); i++)
        {
             weight += edges.get(i).getWeight();
        }
        return weight;
    }

    public void print()
    {
        for (Edge edge: edges
             ) {
            System.out.println(edge.getN0().getLabel() + "->" + edge.getN1().getLabel() + " W: " + edge.getWeight());
        }
    }
}
