import java.util.Comparator;

public class Key_comparator implements Comparator<Node> {

    @Override
    public int compare(Node n0, Node n1)
    {
        /*
        if(n0.getDisc() < n1.getDisc())
        {
            return -1;
        }
        else if(n0.getDisc() > n1.getDisc())
        {
            return 1;
        }
        else {
            return 0;
        }
        */

        return Integer.compare(n0.getDisc(), n1.getDisc());
    }
}
