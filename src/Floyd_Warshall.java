public class Floyd_Warshall {
    public Floyd_Warshall()
    {

    }

    public int[][] run_Floyd_Warshall(Graph_adjazenz_list alist, Graph_edge_list elist, boolean gerichtet)
    {
        int nodecount = elist.getNode_count();
        int w[][] = new int[nodecount][nodecount];
        for(int i = 0; i < nodecount; i++)
        {
            for(int j = 0; j < nodecount; j++)
            {
                int n0, n1;
                n0 = alist.node_lists[i].get(0).getLabel();
                n1 = alist.node_lists[j].get(0).getLabel();
                Edge edge = elist.getEdge(n0, n1, gerichtet);
                if(edge != null)
                {
                    w[i][j] = edge.getWeight();
                }
                else if(i == j)
                {
                    w[i][j] = 0;
                }
                else
                    {
                    w[i][j] = Integer.MAX_VALUE / 3;
                }
            }
        }
/*
        System.out.println();
        for(int i = 0; i < nodecount; i++)
        {
            for(int j = 0; j < nodecount; j++)
            {
                System.out.print(w[i][j] + ", ");
            }
            System.out.println();
        }
        System.out.println();
*/

        int[][] dkm1 = w;
        //int[][] dk;
        int[][] dk = w;
        for(int k = 1; k <= nodecount; k++)
        {
            dk = dkm1;
            for(int i = 0; i < nodecount; i++)
            {
                for(int j = 0; j < nodecount; j++)
                {
                    int v0 = dkm1[i][j];
                    int v1 = dkm1[i][k-1];
                    int v2 = dkm1[k-1][j];

                    if(v0 < v1 + v2)
                    {
                        dk[i][j] = v0;
                    }
                    else
                    {
                        dk[i][j] = v1 + v2;
                    }
                }
            }
            dkm1 = dk;
/*
            for(int i = 0; i < nodecount; i++)
            {
                for(int j = 0; j < nodecount; j++)
                {
                    System.out.print(dk[i][j] + ", ");
                }
                System.out.println();
            }
            System.out.println();
*/
        }



        return dk;
    }
}
