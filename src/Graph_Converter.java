public class Graph_Converter {

    public Graph_Converter()
    {

    }

    public void invertEdgeList(Graph_edge_list graph)
    {
        for (Edge e: graph.edges
             ) {
            Node temp = e.getN0();
            e.setN0(e.getN1());
            e.setN1(temp);
        }
        /*
        {
            System.out.println("\nKantenliste: ");
            System.out.println("Node count: " + graph.getNode_count());
            System.out.println("Edge count: " + graph.edges.size());

            graph.edges.forEach((e) -> System.out.println(e.getN0().getLabel() + " " + e.getN1().getLabel()));
        }
        */
    }

    public Graph_adjazenz_list convertEdgeListToAdjazenzList(Graph_edge_list graph, boolean gerichtet)
    {
        int node_count = graph.getNode_count();
        Graph_adjazenz_list alist = new Graph_adjazenz_list(node_count);

        for(int i = 0; i < node_count; i++)
        {
            alist.node_lists[i].add(new Node(i+1));
        }

        for (Edge e: graph.edges
             ) {
            Node n0, n1;

            n0 = alist.node_lists[e.getN0().getLabel() - 1].get(0);
            n1 = alist.node_lists[e.getN1().getLabel() - 1].get(0);

            alist.node_lists[n0.getLabel() - 1].add(n1);

            if(!gerichtet)
            {
                alist.node_lists[n1.getLabel() - 1].add(n0);
            }
        }
/*
        for (int i = 0; i < node_count; i++)
        {
            int node = i + 1;
            System.out.print("Nodelist: ");
            alist.node_lists[i].forEach((n) -> System.out.print(n.getLabel() + ", "));
            System.out.println();
        }

 */
        return alist;
    }
}
