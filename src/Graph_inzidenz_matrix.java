import java.util.ArrayList;

public class Graph_inzidenz_matrix {
    public int[][] matrix;
    private int node_count;
    private int edge_count;

    public Graph_inzidenz_matrix(int node_count, int edge_count)
    {
        this.node_count = node_count;
        this.edge_count = edge_count;
        this.matrix = new int[node_count][edge_count];
    }

    public int getEdge_count() {
        return edge_count;
    }

    public void setEdge_count(int edge_count) {
        this.edge_count = edge_count;
    }

    public int getNode_count() {
        return node_count;
    }

    public void setNode_count(int node_count) {
        this.node_count = node_count;
    }
}
