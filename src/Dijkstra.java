import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

public class Dijkstra {
    private PriorityQueue<Node> q;

    public Dijkstra()
    {
        Comparator c = new Key_comparator();
        this.q = new PriorityQueue<Node>(c);
    }

    public boolean run_dijkstra(Graph_adjazenz_list alist, Graph_edge_list elist, int source, boolean gerichtet)
    {

        if(source <= alist.getNode_count() && source > 0)
        {
            Node s = alist.node_lists[source-1].get(0);
            s.setDisc(0);
        }
        else
        {
            System.err.println(source + " is not an existing node!");
            return false;
        }

        ArrayList<Node> S = new ArrayList<>();
        for (ArrayList<Node> l: alist.node_lists
             ) {
                q.add(l.get(0));
        }

        while(!q.isEmpty())
        {
            Node u = q.poll();
            S.add(u);
            ArrayList<Node> u_list = alist.node_lists[u.getLabel()-1];
            for(int i = 1; i < u_list.size(); i++)
            {
                Node v = u_list.get(i);
                int w = elist.getEdge(u.getLabel(), v.getLabel(), gerichtet).getWeight();
                Bellmann_Ford bf = new Bellmann_Ford();
                bf.relax(u, v, w);
                if(q.contains(v))
                {
                    q.remove(v);
                    q.add(v);
                }
            }
        }
        return true;
    }
}
