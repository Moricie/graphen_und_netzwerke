import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Graph_parser {

    boolean gerichtet;

    public Graph_parser(boolean gerichtet)
    {
        this.gerichtet = gerichtet;
    }


    private Scanner create_scanner(String path)
    {
        File file = new File(path);
        try
        {
            Scanner scanner = new Scanner(file);
            return scanner;
        }
        catch (IOException ex)
        {
            System.err.println(ex.getMessage());
            return null;
        }
    }


    private int getNode_count(Scanner sc)
    {
        int node_count = 0;
        if (sc.hasNextInt())
        {
            node_count = sc.nextInt();
        }
        return node_count;
    }



    private int getEdge_count(String path)
    {
        Scanner sc = create_scanner(path);
        int edge_count = 0;

        while (sc.hasNextLine())
        {
            edge_count++;
            sc.nextLine();
        }
        // First line contains node_count no edge
        return edge_count - 1;
    }


    public Graph_edge_list init_edge_list(String path, boolean weighted,boolean with_output)
    {

        Scanner sc = create_scanner(path);

        int node_count = getNode_count(sc);

        Graph_edge_list list = new Graph_edge_list(node_count);

        while(sc.hasNext())
        {
            if (sc.hasNextInt())
            {
                Node n0, n1;
                int weight;

                n0 = new Node(sc.nextInt());
                if(weighted)
                {
                    weight = sc.nextInt();
                }
                else
                {
                    weight = 0;
                }
                n1 = new Node(sc.nextInt());

                Edge e = new Edge(n0, weight, n1);

                if(list.edges.size() == 0)
                {
                    list.edges.add(e);
                }
                else {

                    for (int i = 0; i < list.edges.size(); i++) {
                        Edge edge = list.edges.get(i);
                        if (e.getWeight() == edge.getWeight()) {
                            list.edges.add(i + 1, e);
                            break;
                        } else if (i < 2 && e.getWeight() < edge.getWeight()) {
                            list.edges.add(i, e);
                            break;
                        } else if (i < list.edges.size() - 1 && e.getWeight() > edge.getWeight() && e.getWeight() < list.edges.get(i + 1).getWeight()) {
                            list.edges.add(i + 1, e);
                            break;
                        } else if (i < list.edges.size() - 1 && e.getWeight() < edge.getWeight() && e.getWeight() < list.edges.get(i + 1).getWeight()) {
                            list.edges.add(i + 1, e);
                            break;
                        } else if (i == list.edges.size() - 1) {
                            list.edges.add(e);
                            break;
                        }

                    }
                }

            }
        }

        if(with_output)
        {
            System.out.println("\nKantenliste: ");
            System.out.println("Node count: " + node_count);
            System.out.println("Edge count: " + getEdge_count(path));

            list.edges.forEach((e) -> System.out.println(e.getN0().getLabel() + " " + e.getN1().getLabel()));
        }


        sc.close();

        return list;
    }

    public Graph_inzidenz_matrix init_inzidenz_matrix(String path, boolean weighted, boolean with_output)
    {
        Scanner sc = create_scanner(path);

        int node_count = getNode_count(sc);
        int edge_count = getEdge_count(path);

        Graph_inzidenz_matrix matrix = new Graph_inzidenz_matrix(node_count, edge_count);

        int edgeind = 0;
        while(sc.hasNext())
        {
            if (sc.hasNextInt())
            {
                int n0, n1 = 0;
                int weight;

                n0 = sc.nextInt();

                if(weighted)
                {
                    weight = sc.nextInt();
                }
                else
                {
                    weight = 0;
                }
                n1 = sc.nextInt();

                matrix.matrix[n0 - 1][edgeind] = 1;
                matrix.matrix[n1 - 1][edgeind] = 1;
                edgeind++;
            }
        }

        if(with_output)
        {
            System.out.println("\nInzidenzmatrix: ");
            System.out.println("Node count: " + node_count);
            System.out.println("Edge count: " + edge_count);

            for (int j = 0; j < node_count; j++)
            {
                for (int i = 0; i < edge_count; i++)
                {
                    System.out.print(matrix.matrix[j][i] + ", ");
                }
                System.out.println();
            }
        }

        sc.close();

        return matrix;
    }

    public Graph_adjazenz_matrix init_adjazenz_matrix(String path, boolean weighted, boolean with_output)
    {
        Scanner sc = create_scanner(path);

        int node_count = getNode_count(sc);

        Graph_adjazenz_matrix matrix = new Graph_adjazenz_matrix(node_count);

        while(sc.hasNext())
        {
            if (sc.hasNextInt())
            {
                int n0, n1;
                int weight;
                n0 = sc.nextInt();
                if(weighted)
                {
                     weight = sc.nextInt();
                }
                else
                {
                    weight = 0;
                }
                n1 = sc.nextInt();

                matrix.matrix[n0 - 1][n1 - 1] = 1;
                if(!gerichtet)
                {
                    matrix.matrix[n1 - 1][n0 - 1] = 1;
                }
            }
        }

        if(with_output) {
            System.out.println("\nAdjazenzmatrix: ");
            System.out.println("Node count: " + node_count);
            System.out.println("Edge count: " + getEdge_count(path));

            for (int j = 0; j < node_count; j++)
            {
                for (int i = 0; i < node_count; i++)
                {
                    System.out.print(matrix.matrix[j][i] + ", ");
                }
                System.out.println();
            }
        }

        sc.close();

        return matrix;
    }

    public Graph_adjazenz_list init_adjazenz_list(String path, boolean weighted, boolean with_output)
    {
        Scanner sc = create_scanner(path);

        int node_count = getNode_count(sc);
        Graph_adjazenz_list list = new Graph_adjazenz_list(node_count);

        for(int i = 0; i < node_count; i++)
        {
            list.node_lists[i].add(new Node(i+1));
        }

        while(sc.hasNext())
        {
            if (sc.hasNextInt())
            {
                Node n0, n1;
                int weight;
                n0 = list.node_lists[sc.nextInt() - 1].get(0);
                if(weighted)
                {
                    weight = sc.nextInt();
                }
                else
                {
                    weight = 0;
                }
                n1 = list.node_lists[sc.nextInt() - 1].get(0);

                list.node_lists[n0.getLabel() - 1].add(n1);
                if(!gerichtet)
                {
                    list.node_lists[n1.getLabel() - 1].add(n0);
                }
            }
        }

        if(with_output)
        {
            System.out.println("\nAdjazenzliste: ");
            System.out.println("Node count: " + node_count);
            System.out.println("Edge count: " + getEdge_count(path));

            for (int i = 0; i < node_count; i++)
            {
                int node = i + 1;
                System.out.print("Nodelist: ");
                list.node_lists[i].forEach((n) -> System.out.print(n.getLabel() + ", "));
                System.out.println();
            }
        }
        sc.close();

        return list;
    }
}
