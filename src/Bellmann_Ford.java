public class Bellmann_Ford {

    public Bellmann_Ford()
    {

    }

    public boolean run_Bellmann_Ford(Graph_adjazenz_list alist, Graph_edge_list elist, int source)
    {

        if(source <= alist.getNode_count() && source > 0)
        {
            Node s = alist.node_lists[source-1].get(0);
            s.setDisc(0);
        }
        else
        {
            System.err.println(source + " is not an existing node!");
            return false;
        }


        for(int i = 1; i < alist.getNode_count(); i++)
        {
            for (Edge e : elist.edges
            ) {
                Node u, v;
                int w;
                u = alist.node_lists[e.getN0().getLabel() - 1].get(0);
                v = alist.node_lists[e.getN1().getLabel() - 1].get(0);
                w = e.getWeight();
                relax(u, v, w);
            }
        }
        for(int i = 1; i < alist.getNode_count(); i++)
            for (Edge e: elist.edges
            ) {
                Node u, v;
                int w;
                u = alist.node_lists[e.getN0().getLabel()-1].get(0);
                v = alist.node_lists[e.getN1().getLabel()-1].get(0);
                w = e.getWeight();
                if(v.getDisc() > u.getDisc() + w)
                {
                    return false;
                }
            }
        return true;
    }

    public void relax(Node u, Node v, int w)
    {
        int v_d, u_d;
        v_d = v.getDisc();
        u_d = u.getDisc();
        if(v_d > u_d + w)
        {
            v.setDisc(u_d + w);
            v.setPred(u);
        }

    }
}
