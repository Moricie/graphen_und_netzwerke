import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Dot_Generator {

    boolean gerichtet;
    // If the nodes should be labeled with letters
    char labels[] = {'O', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
                     'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Z'};

    public Dot_Generator(boolean gerichtet)
    {
        this.gerichtet = gerichtet;
    }

    private void write_to_file(String name, StringBuilder sb)
    {
        PrintWriter pWriter = null;
        try {
            pWriter = new PrintWriter(new BufferedWriter(new FileWriter(name + ".txt")));

            pWriter.println(sb);

        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            if (pWriter != null){
                pWriter.flush();
                pWriter.close();
            }
        }
    }

    public void gen_for_edge_list(Graph_edge_list list, String name)
    {

        StringBuilder sb = new StringBuilder();

        sb.append("digraph D {\n");
        sb.append(System.getProperty("line.separator"));
        for(Edge e : list.edges)
        {

            if(!gerichtet)
            {
                sb.append(e.getN0().getLabel() + " -> " + e.getN1().getLabel());
                sb.append(System.getProperty("line.separator"));
            }
            else
            {
                sb.append(e.getN0().getLabel() + " -> " + e.getN1().getLabel() + " [arrowhead=none]");
                sb.append(System.getProperty("line.separator"));
            }
        }
        sb.append("\n}");
        sb.append(System.getProperty("line.separator"));

        String new_name = name + "-edge_list";

        write_to_file(new_name, sb);
    }

    public void gen_for_inzidenz_matrix(Graph_inzidenz_matrix matrix, String name)
    {
        StringBuilder sb = new StringBuilder();

        sb.append("digraph D {\n");
        sb.append(System.getProperty("line.separator"));

        for(int j = 0; j < matrix.getNode_count(); j++)
        {
            for(int i = 0; i < matrix.getEdge_count(); i++)
            {
                int node0 = j+1;

                if(matrix.matrix[j][i] == 1)
                {
                    for(int k = j+1; k < matrix.getNode_count(); k++)
                    {
                        int node1 = k+1;
                        if(matrix.matrix[k][i] == 1)
                        {
                            if(!gerichtet)
                            {

                                sb.append(node0 + " -> " + node1 + " [arrowhead=none]");
                                sb.append(System.getProperty("line.separator"));
                            }
                            else
                            {
                                sb.append(node0 + " -> " + node1);
                                sb.append(System.getProperty("line.separator"));
                            }
                        }


                    }

                }

            }
        }

        sb.append("\n}");
        sb.append(System.getProperty("line.separator"));

        String new_name = name + "-inzidenz_matrix";

        write_to_file(new_name, sb);
    }

    public void gen_for_adjazenz_matrix(Graph_adjazenz_matrix matrix, String name)
    {
        StringBuilder sb = new StringBuilder();

        sb.append("digraph D {\n");
        sb.append(System.getProperty("line.separator"));

        int node_count = matrix.getNode_count();

        for(int j = 0; j < node_count; j++)
        {
            for(int i = 0; i < node_count; i++)
            {
                int node0 = j+1;
                int node1 = i+1;

                if(matrix.matrix[j][i] == 1)
                {
                    if(!gerichtet)
                    {
                        if(i < j)
                        {
                            sb.append(node0 + " -> " + node1 + " [arrowhead=none]");
                            sb.append(System.getProperty("line.separator"));
                        }
                    }
                    else
                    {
                        sb.append(node0 + " -> " + node1);
                        sb.append(System.getProperty("line.separator"));
                    }

                }

            }

        }

        sb.append("\n}");
        sb.append(System.getProperty("line.separator"));

        String new_name = name + "-adjazenz_matrix";

        write_to_file(new_name, sb);
    }

    public void gen_for_adjazenz_list(Graph_adjazenz_list list, String name)
    {
        StringBuilder sb = new StringBuilder();

        sb.append("digraph D {\n");
        sb.append(System.getProperty("line.separator"));
        for(int i = 0; i < list.node_lists.length; i++)
        {
            int node = i+1;
            for(Node n : list.node_lists[i])
            {
                if(!gerichtet)
                {
                    if(n.getLabel() > node)
                    {
                        sb.append(node + " -> " + n.getLabel() + " [arrowhead=none]");
                        sb.append(System.getProperty("line.separator"));
                    }
                }
                else
                {
                    sb.append(node + " -> " + n.getLabel());
                    sb.append(System.getProperty("line.separator"));
                }
            }
        }
        sb.append("\n}");
        sb.append(System.getProperty("line.separator"));

        String new_name = name + "-adjazenz_liste";

        write_to_file(new_name, sb);
    }
}
