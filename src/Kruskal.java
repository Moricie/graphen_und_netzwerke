import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Kruskal {

    public Kruskal()
    {

    }

    public Graph_edge_list runKruskal(Graph_edge_list elist, Graph_adjazenz_list alist)
    {
        UnionFind<Integer> sets = new UnionFind<Integer>(new HashSet<Integer>());
        Graph_edge_list spanning_tree = new Graph_edge_list(elist.getNode_count());
        for(int i = 0; i < alist.getNode_count(); i++)
        {
            sets.addElement(alist.node_lists[i].get(0).getLabel());
        }
        for (Edge edge : elist.edges) {
            if(!sets.inSameSet(edge.getN0().getLabel(), edge.getN1().getLabel()))
            {
                spanning_tree.edges.add(edge);
                sets.union(edge.getN0().getLabel(), edge.getN1().getLabel());
            }
        }

        return spanning_tree;
    }

}
