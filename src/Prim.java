import java.util.Comparator;
import java.util.PriorityQueue;

public class Prim {
    private PriorityQueue<Node> q;

    public Prim()
    {
        Comparator<Node> key_comparator = new Key_comparator();
        this.q = new PriorityQueue<Node>(key_comparator);
    }

    public void runPrim(Graph_edge_list elist, Graph_adjazenz_list alist, boolean gerichtet)
    {
        Node r = alist.node_lists[0].get(0);
        r.setDisc(0);
        r.setVisited(true);
        q.add(r);
        for(int i = 1; i < alist.getNode_count(); i++)
        {
            q.add(alist.node_lists[i].get(0));
        }
        while(!q.isEmpty())
        {
            Node u = q.poll();
            for(int i = 1; i < alist.node_lists[u.getLabel()-1].size(); i++)
            {
                Node v = alist.node_lists[u.getLabel()-1].get(i);
                int edge_weight = elist.getEdge(u.getLabel(), v.getLabel(), gerichtet).getWeight();
                if (edge_weight < v.getDisc())
                {
                    if(!v.isVisited())
                    {
                        v.setPred(u);
                        v.setDisc(edge_weight);
                        if(q.contains(v))
                        {
                            q.remove(v);
                            q.add(v);
                        }
                    }

                }

            }
        }
    }
}
