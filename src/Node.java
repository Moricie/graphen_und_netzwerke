public class Node {
    private int label;
    private boolean visited;
    private Node pred;
    private int disc; // Discovery time or distance
    private int finish; // Finish time

    public Node(int label)
    {
        this.label = label;
        this.visited = false;
        this.pred = null;
        this.disc = Integer.MAX_VALUE / 3;
        this.finish = Integer.MAX_VALUE / 3;
    }

    public int getDisc() {
        return disc;
    }

    public int getFinish() {
        return finish;
    }

    public int getLabel() {
        return label;
    }

    public Node getPred() {
        return pred;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setPred(Node pred) {
        this.pred = pred;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public void setDisc(int disc) {
        this.disc = disc;
    }

    public void setFinish(int finish) {
        this.finish = finish;
    }

    public void setLabel(int label) {
        this.label = label;
    }
}
