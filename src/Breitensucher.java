import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class Breitensucher {

    private Queue<Node> q;

    public Breitensucher()
    {
        q = new LinkedList<Node>();
    }

    public void breitensuche(Graph_adjazenz_list list)
    {
        for(ArrayList<Node> l : list.node_lists)
        {

            Node s = l.get(0);
            if(!s.isVisited())
            {
                s.setVisited(true);
                s.setDisc(0);
                q.add(s);
            }
            while(!q.isEmpty())
            {
                Node u = q.poll();
                for (Node node : list.node_lists[u.getLabel()-1])
                {
                    if(!node.isVisited()) {
                        node.setVisited(true);
                        node.setDisc(u.getDisc() + 1);
                        node.setPred(u);
                        q.add(node);
                    }

                }
                u.setVisited(true);
            }

        }
        // Output
        for(ArrayList<Node> l : list.node_lists)
        {
            Node n = l.get(0);
            if(n.getPred() != null)
            {
                System.out.println(n.getLabel() + ", D: " + n.getDisc() + ", Pred: " + n.getPred().getLabel());
            }
        }

    }
}
