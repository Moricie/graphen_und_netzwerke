public class Edge {
    private Node n0;
    private Node n1;
    private int weight;

    public Edge(Node n0, int weight, Node n1)
    {
        this.n0 = n0;
        this.n1 = n1;
        this.weight = weight;
    }

    public Node getN0()
    {
        return n0;
    }

    public Node getN1()
    {
        return n1;
    }

    public void setN0(Node n0)
    {
        this.n0 = n0;
    }

    public void setN1(Node n1)
    {
        this.n1 = n1;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
