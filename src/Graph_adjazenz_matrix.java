import java.util.ArrayList;

public class Graph_adjazenz_matrix {
    private int node_count;
    public int[][] matrix;

    public Graph_adjazenz_matrix(int node_count)
    {
        this.node_count = node_count;
        this.matrix = new int[node_count][node_count];
    }

    public int getNode_count() {
        return node_count;
    }
}
