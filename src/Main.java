import java.lang.reflect.GenericArrayType;
import java.util.ArrayList;

public class Main
{
    public static void main(String[] args)
    {
        // Should the stuff (Graph_parser) be printed out?
        final boolean WITH_OUTPUT = false;
        final boolean GERICHTET = true;
        boolean WEIGHTED = true;

        String path = "graphs/grid.txt";
        String graph_name = path.substring(path.lastIndexOf('/')+1, path.lastIndexOf('.'));
        String dot_path = "dot_files/" + graph_name;

        switch (graph_name)
        {
            case "breiten_tiefen_suche":
            case "k3_3":
            case "k5":
            case "petersen":
                WEIGHTED = false;
                break;

            case "primkruskal":
            case "bellmannford":
            case "FloydWarshall":
            case "dijkstra":
            case "ladder":
            case "grid":
            case "spider":
                default:
                    WEIGHTED = true;
                    break;
        }

        Graph_parser gp = new Graph_parser(GERICHTET);

        // Save graph in different data structures
        Graph_edge_list elist = gp.init_edge_list(path, WEIGHTED, WITH_OUTPUT);

        Graph_inzidenz_matrix imat = gp.init_inzidenz_matrix(path, WEIGHTED, WITH_OUTPUT);

        Graph_adjazenz_matrix amat = gp.init_adjazenz_matrix(path, WEIGHTED, WITH_OUTPUT);

        Graph_adjazenz_list alist = gp.init_adjazenz_list(path, WEIGHTED, WITH_OUTPUT);


        // Generate dot files from different data structures
        Dot_Generator gen = new Dot_Generator(GERICHTET);

        gen.gen_for_edge_list(elist, dot_path);

        gen.gen_for_adjazenz_list(alist, dot_path);

        gen.gen_for_inzidenz_matrix(imat, dot_path);

        gen.gen_for_adjazenz_matrix(amat, dot_path);

        Breitensucher bs = new Breitensucher();
        Tiefensucher ts = new Tiefensucher();
        Graph_Converter converter = new Graph_Converter();
        Kruskal kruskal = new Kruskal();
        Prim prim = new Prim();
        Bellmann_Ford bf = new Bellmann_Ford();
        Floyd_Warshall fl = new Floyd_Warshall();
        Dijkstra d = new Dijkstra();


        ArrayList<ArrayList<Node>> top_sorts;
        Graph_adjazenz_list invertedList;
        Graph_edge_list spanning_tree;
        int source = 1;
        int[][] mat_floyd_warshall;


        boolean no_neg_circ;

        switch (graph_name)
        {
            case "k3_3":
            case "k5":
            case "petersen":
            case "breiten_tiefen_suche":


                bs.breitensuche(alist);
                alist = gp.init_adjazenz_list(path, WEIGHTED, WITH_OUTPUT);
                top_sorts = ts.tiefensuche(alist);

                converter.invertEdgeList(elist);
                invertedList = converter.convertEdgeListToAdjazenzList(elist, GERICHTET);
                ts.starke_z_k(invertedList, top_sorts);
                break;

            case "primkruskal":

                // Kruskal spanning tree
                alist = gp.init_adjazenz_list(path, WEIGHTED, WITH_OUTPUT);
                elist = gp.init_edge_list(path, WEIGHTED, WITH_OUTPUT);

                spanning_tree = kruskal.runKruskal(elist, alist);
                System.out.println("Minimal spanning tree Kruskal");
                spanning_tree.print();


                // Prim spanning tree
                alist = gp.init_adjazenz_list(path, WEIGHTED, WITH_OUTPUT);
                elist = gp.init_edge_list(path, WEIGHTED, WITH_OUTPUT);

                prim.runPrim(elist, alist, GERICHTET);
                System.out.println("\nMinimal spanning tree through preds with Prim.");
                alist.print_preds();

                break;

            case "bellmannford":

                alist = gp.init_adjazenz_list(path, WEIGHTED, WITH_OUTPUT);
                elist = gp.init_edge_list(path, WEIGHTED, WITH_OUTPUT);


                no_neg_circ = bf.run_Bellmann_Ford(alist, elist, source);
                System.out.println("Bellman Ford shortest paths for Node: " + source + " Contains no negative circle: " + no_neg_circ);
                alist.print_preds();
                break;

            case "FloydWarshall":

                alist = gp.init_adjazenz_list(path, WEIGHTED, WITH_OUTPUT);
                elist = gp.init_edge_list(path, WEIGHTED, WITH_OUTPUT);

                mat_floyd_warshall = fl.run_Floyd_Warshall(alist, elist, GERICHTET);
                for(int i = 0; i < mat_floyd_warshall.length; i++)
                {
                    for(int j = 0; j < mat_floyd_warshall[i].length; j++)
                    {
                        System.out.print(mat_floyd_warshall[i][j] + " ");
                    }
                    System.out.println();
                }
                break;

            case "dijkstra":

                alist = gp.init_adjazenz_list(path, WEIGHTED, WITH_OUTPUT);
                elist = gp.init_edge_list(path, WEIGHTED, WITH_OUTPUT);

                d.run_dijkstra(alist, elist, source, GERICHTET);
                alist.print_preds();
                break;


            case "ladder":
            case "spider":
            case "grid":
                /*
                System.out.println("Breitensuche");
                bs.breitensuche(alist);

                System.out.println("Tiefensuche");
                alist = gp.init_adjazenz_list(path, WEIGHTED, WITH_OUTPUT);
                top_sorts = ts.tiefensuche(alist);

                converter.invertEdgeList(elist);
                invertedList = converter.convertEdgeListToAdjazenzList(elist, GERICHTET);
                ts.starke_z_k(invertedList, top_sorts);


                // Kruskal spanning tree
                System.out.println("Kruskal");
                alist = gp.init_adjazenz_list(path, WEIGHTED, WITH_OUTPUT);
                elist = gp.init_edge_list(path, WEIGHTED, WITH_OUTPUT);

                spanning_tree = kruskal.runKruskal(elist, alist);
                System.out.println("Minimal spanning tree Kruskal");
                spanning_tree.print();


                // Prim spanning tree
                System.out.println("Prim");
                alist = gp.init_adjazenz_list(path, WEIGHTED, WITH_OUTPUT);
                elist = gp.init_edge_list(path, WEIGHTED, WITH_OUTPUT);

                prim.runPrim(elist, alist, GERICHTET);
                alist.print_preds();
*/

/*
                // Bellman_Ford
                System.out.println("Bellman_Ford");
                alist = gp.init_adjazenz_list(path, WEIGHTED, WITH_OUTPUT);
                elist = gp.init_edge_list(path, WEIGHTED, WITH_OUTPUT);

                no_neg_circ = bf.run_Bellmann_Ford(alist, elist, source);
                System.out.println("Bellman Ford shortest paths for Node: " + source + " Contains no negative circle: " + no_neg_circ);
                alist.print_preds();


*/
                // Dijkstra
                System.out.println("Dijkstra");
                alist = gp.init_adjazenz_list(path, WEIGHTED, WITH_OUTPUT);
                elist = gp.init_edge_list(path, WEIGHTED, WITH_OUTPUT);

                d.run_dijkstra(alist, elist, source, GERICHTET);
                alist.print_preds();



/*

                // Floyd Warshall
                System.out.println("Floyd Warshall");
                alist = gp.init_adjazenz_list(path, WEIGHTED, WITH_OUTPUT);
                elist = gp.init_edge_list(path, WEIGHTED, WITH_OUTPUT);

                mat_floyd_warshall = fl.run_Floyd_Warshall(alist, elist, GERICHTET);
                for(int i = 0; i < mat_floyd_warshall.length; i++)
                {
                    for(int j = 0; j < mat_floyd_warshall[i].length; j++)
                    {
                        System.out.print(mat_floyd_warshall[i][j] + " ");
                    }
                    System.out.println();
                }


                break;
*/

                default:
                    break;
        }
















    }

}
